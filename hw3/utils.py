import json
import random
import string

import requests
from faker import Faker

CENTIMETER_PER_INCH = 2.54
KILOGRAMS_PER_POUND = 0.453592


def convert_inches_to_centimeters(inches):
    return inches * CENTIMETER_PER_INCH


def convert_pounds_to_kilograms(pounds):
    return pounds * KILOGRAMS_PER_POUND


def get_random_string(length, chars=False):
    if chars:
        letters = string.ascii_letters + string.digits
    else:
        letters = string.digits
    return ''.join(random.choice(letters) for _ in range(length))


def return_rates_for_bitcoins():
    response = requests.get(url="https://bitpay.com/api/rates")
    rates = json.loads(response.text)
    return rates


def generate_user_data_for_customer():
    faker = Faker()
    data = {
        "FirstName": faker.first_name(),
        'LastName': faker.last_name(),
        "Email": faker.email(),
        "Company": faker.company(),
        "Address": faker.address(),
        "City": faker.city(),
        "Country": faker.country(),
        "PostalCode": faker.postcode(),
        "Phone": faker.phone_number()
    }
    return data


def is_integer(n):
    try:
        float(n)
    except ValueError:
        return False
    else:
        return float(n).is_integer()
