import csv
from sqlite3 import OperationalError

from faker import Faker
from flask import Flask, request, Response, render_template

from db_class import DataBaseAccess
from utils import convert_pounds_to_kilograms, convert_inches_to_centimeters, get_random_string, \
    return_rates_for_bitcoins, generate_user_data_for_customer, is_integer

db_executor = DataBaseAccess()
app = Flask(__name__)


@app.route('/')
@app.route('/index')
def index():
    links = {"/requirements": "Requirements",
             "/random_users": " Random users",
             "/avr_data": "Avg calculation",
             "/password": "Generate passwords",
             "/bitcoin": "Bitcoin rate",
             "/unique_names": "Unique names from db",
             "/gross_turnover": "Company turnover",
             "/tracks_count": "Records count",
             "/customers": "Customers table",
             "/genres": "Tracks lengths in tracks table grouped by generes",
             "/generate_customer": "Generate customer",
             "/generate_customers": "Generate some customers"}
    return render_template("index.html", links=links)


@app.route('/requirements')
def get_requirements():
    with open("requirements.txt") as req:
        reader = req.readlines()
        list_fo_template = {}
        for line in reader:
            line = line.replace("~=", "==")
            split = line.split("==")
            list_fo_template[split[0]] = split[1]
    return render_template("requirements.html", reader=list_fo_template)


@app.route('/random_users')
def gen_random_users():
    faker = Faker()
    output = list()
    for i in range(100):
        output.append((str(i + 1), faker.name(), faker.ascii_email()))
    return render_template("random_users.html", data=output)


@app.route("/avr_data")
def get_avr_data():
    file = open("files/hw.csv")
    csv_dicts = csv.DictReader(file)
    height = 0.0
    weight = 0.0
    for count, row in enumerate(csv_dicts):
        height += float(row['Height(Inches)'])
        weight += float(row['Weight(Pounds)'])
    return render_template("avr_data.html", height=height, weight=weight, count=count,
                           convert_inches_to_centimeters=convert_inches_to_centimeters,
                           convert_pounds_to_kilograms=convert_pounds_to_kilograms)


@app.route("/password")
def get_random():
    need_letters = request.args.get('chars', '0')
    if need_letters not in ('0', '1'):
        return Response("Incorrect value of chars parameter", status=400)
    length = request.args.get('length', str(10))
    if not 6 < int(length) <= 50:
        return Response("Length should be in [6 .. 50]", status=400)
    return render_template("password.html", password=get_random_string(int(length), bool(int(need_letters))),
                           need_letters=need_letters, length=length)


@app.route("/bitcoin")
def get_bitcoin_rate():
    currency = request.args.get('currency', 'USD')
    rates = return_rates_for_bitcoins()
    available_rates = 'Available rates codes: ' + ', '.join(rate['code'] for rate in rates)
    for rate in rates:
        if rate['code'] == currency:
            return available_rates + '<p>The rate of bitcoin for currency ' + currency + ' is ' + str(
                rate['rate']) + '<p><a href="/"> To index </a>'
    return Response("Currency " + currency + " not found", status=400)


@app.route('/unique_names')
def get_unique_names():
    return render_template("unique_names.html", count=db_executor.get_unique_names_count(),
                           rows=db_executor.get_short_info_from_customer_table())


@app.route('/gross_turnover')
def get_gross_turnover():
    return 'The company turnover is ' + str(db_executor.get_gross_turnover()) + '<p><a href="/"> To index </a>'


@app.route('/tracks_count')
def get_tracks_count():
    return 'The count of records from "tracks" table is ' + str(db_executor.get_tracks_count()) + \
           '<p><a href="/"> To index </a>'


@app.route('/customers')
def get_customers():
    city = request.args.get('city', '')
    country = request.args.get('country', '')
    rows = db_executor.get_customers_by_city_and_country(city, country)
    return render_template("customers.html", rows=rows, names=db_executor.get_table_columns_names("customers"))


@app.route('/genres')
def get_genres():
    rows = db_executor.get_tracks_length_group_by_generes()
    return render_template("ganres.html", rows=rows)


@app.route('/generate_customer')
def generate_customer():
    data = generate_user_data_for_customer()
    keys = tuple(data.keys())
    vals = tuple(data.values())
    try:
        db_executor.generate_customer(keys, vals)
        return render_template("generate_customer.html", headers=keys, values=vals)
    except OperationalError as err:
        return ''.join('DB error: {}'.format(err))


@app.route('/generate_customers')
def generate_customers():
    count = request.args.get("count")
    if count is None:
        return "Please specify 'count' parameter"
    if not is_integer(count):
        return "Please specify 'count' parameter as integer"
    if not 0 < int(count) <= 100:
        return "Count should be in range (0 .. 100]"
    result = ''
    for _ in range(int(count)):
        result += " " + generate_customer()
    return result


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=10080)
