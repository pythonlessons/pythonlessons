import os
import sqlite3


class DataBaseAccess:
    def __init__(self):
        self.db_path = os.path.join(os.getcwd(), 'db/chinook.db')
        self.__connection = sqlite3.connect(self.db_path, check_same_thread=False)
        self.__cur = self.__connection.cursor()

    def execute_query(self, query):
        self.__cur.execute(query)
        records = self.__cur.fetchall()
        return records

    def execute_insert(self, sql_query):
        self.__cur.execute(sql_query)
        self.__connection.commit()
        return self.__cur.lastrowid

    def get_gross_turnover(self):
        query = 'SELECT sum(UnitPrice * Quantity) as SUM FROM invoice_items'
        return self.execute_query_with_one_result(query)

    def get_unique_names_count(self):
        query = 'SELECT count() as count FROM (SELECT DISTINCT FirstName FROM customers)'
        return self.execute_query_with_one_result(query)

    def get_short_info_from_customer_table(self):
        """Gets short info in Customer table
           id, First name, Last name """
        query = "SELECT CustomerId, FirstName, LastName FROM customers"
        return self.execute_query(query)

    def get_tracks_count(self):
        query = 'SELECT count(*) as count FROM tracks'
        return self.execute_query_with_one_result(query)

    def execute_query_with_one_result(self, query):
        return self.execute_query(query)[0][0]

    def get_customers_by_city_and_country(self, city='', country=''):
        query = 'SELECT * FROM customers WHERE Country like "{}%" and City like "{}%"'.format(country, city)
        return self.execute_query(query)

    def get_tracks_length_group_by_generes(self):
        query = 'SELECT sum(Milliseconds) / 1000, genres.Name FROM tracks ' \
                'INNER JOIN genres On genres.GenreId=tracks.GenreId GROUP by tracks.GenreId'
        return self.execute_query(query)

    def generate_customer(self, keys, values):
        query = "INSERT INTO customers " + str(keys) + \
                "VALUES " + str(values)
        return self.execute_insert(query)

    def get_table_columns_names(self, table_name):
        query = "SELECT name FROM PRAGMA_TABLE_INFO('{}');".format(table_name)
        return self.execute_query(query)
